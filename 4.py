placa = ""
while True:
    placa = input("Ingrese la placa: ")
    if len(placa) != 5 or not placa.isdigit():
        print("Placa no válida")
    else:
        break
primer_digito = placa[0]
tipo_vehiculo = "Desconocido"

if primer_digito == "1":
    tipo_vehiculo = "Toyota"
elif primer_digito == "2":
    tipo_vehiculo = "Nissan"
elif primer_digito == "3":
    tipo_vehiculo = "Hyundai"
elif primer_digito == "4":
    tipo_vehiculo = "Maseratti"
elif primer_digito == "5":
    tipo_vehiculo = "Mercedes-Benz"
elif primer_digito == "6":
    tipo_vehiculo = "BMW"
elif primer_digito == "7":
    tipo_vehiculo = "Morris Garaje"
elif primer_digito == "8":
    tipo_vehiculo = "Alfa Romeo"
elif primer_digito == "9":
    tipo_vehiculo = "Tesla"
print("La placa es de un vehículo " + tipo_vehiculo)
