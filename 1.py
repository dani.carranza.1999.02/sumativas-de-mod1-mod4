import sys
intentos = 0
maximo_intentos = 10
frase_correcta = "Ábrete Sésamo"

nombre_ladron = input("Tu nombre: ")

while intentos < maximo_intentos:
    frase = input("Ingresa la frase para abrir la puerta: ")
    if frase == frase_correcta:
        print("La puerta se ha abierto")
        sys.exit()
    else:
        print("Incorrecto")
    intentos += 1
        print("La puerta se ha cerrado para siempre")
